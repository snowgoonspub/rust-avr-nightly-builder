# Rust AVR Nightly toolchain builder
A simple script that will download the latest Rust nightly sources, apply
some patches specifically for AVR targets, and then build a local copy
of the toolchain.
## Why is this needed?
It isn't!  Thanks to [Patryk27], since 9th May 2022 the regular Rust `nightly`
toolchain works on AVR again!

> `snowgoons/avr-rustbuilder` Docker images from 10th May 2022 will no
> longer contain the `snowgoons` toolchain.  It *will* contain the `nightly`
> toolchain (so I have a "known working" version of nightly) though, so
> I will continue to occasionally build it.

# For historical interest only...
Rust-on-AVR development came to a juddering halt on 7th January 2021, when
[a bug] was introduced in LLVM that all but makes AVR development impossible.

A [patch] for this was submitting for LLVM by [dylanmckay], but for whatever
reason it's been sitting in limbo ever since.  And ever since then, Rust
developers for AVR have been stuck using an increasingly out of date LLVM,
with an increasingly out of date Rust toolchain as well.

The easiest solution to let you move on and at least use the latest Rust
nightly toolchain is to build the toolchain yourself, manually applying the
necessary patch to get us past this roadblock.

This script automates that process somewhat.

## What this does not do
This will *not magically give you a useful Rust-on-AVR toolchain*.  *Rust/LLVM
for AVR is a _hot mess of code generation bugs_*.  With the latest nightly
toolchain, at least you'll be dealing with exciting new LLVM bugs and not
boring old ones, though.

## Updates
Since we're here, I may as well add other relevant AVR LLVM patches as and when
they appear.  At the time of writing this script includes:

* The aforementioned [patch] from [dylanmckay]
* A patch to re-apply a [regalloc workaround] patch that was recently (Jan 2022)
  removed from LLVM without all the underlying bugs apparently being fixed.

### No longer included
The following patches are no longer included because they have been
accepted into upstream LLVM or are otherwise obsolete:
* [This](https://reviews.llvm.org/D116551) patch for bad register saving in Interrupt Service Routines

# Usage
The `build.sh` script will download the current nightly sources, patch them,
and then compile.  It will then install this as a new toolchain using the
`rustup toolchain link` command.  You can then use this toolchain in your
`rust-toolchain.toml` file to build for AVR with a current, if buggy, toolchain.

By default, the new toolchain will be named `snowgoons`.  You can give an
alternative name as a parameter to the script.

By default, if the script finds the sources are already there, it will do a
`git pull` to update them before building.  If you wish to suppress this,
you can pass a `--noupdate` flag to the script.

In summary:
```
build.sh usage:
  build.sh [--noupdate] [<toolchain-name>]
    
  --noupdate:       Do not update to latest Rust sources if they are already present
  <toolchain-name>: Use <toolchain-name> instead of the default ('snowgoons')
                    as the name of the newly built toolchain.
```

## Prerequisites
You will need all the prerequisites required to build the Rust toolchain.
You can see what these are in the [rust sources README.md file](https://github.com/rust-lang/rust),
but at the time of writing they are:

   * `g++` 5.1 or later or `clang++` 3.5 or later
   * `python` 3 or 2.7
   * GNU `make` 3.81 or later
   * `cmake` 3.13.4 or later
   * `ninja`
   * `curl`
   * `git`
   * `ssl` which comes in `libssl-dev` or `openssl-devel`
   * `pkg-config` if you are compiling on Linux and targeting Linux

# Docker Images
I will periodically build and push a Docker image up to the public Docker Hub
repo with a built version of the toolchain, named `snowgoons/avr-rustbuilder:nightly-YYYY-MM-DD`
(where `YYYY-MM-DD` is the date the toolchain was built, hopefully
corresponding to the verion of nightly available at that time!).  This is to
allow for reliable builds in CI/CD of my [AVRoxide] AVR operating system
project, but can of course also be used by anyone else who so wishes.


[a bug]: https://github.com/rust-lang/compiler-builtins/issues/400
[patch]: https://reviews.llvm.org/D95664
[regalloc workaround]: https://reviews.llvm.org/D117831
[dylanmckay]: https://reviews.llvm.org/p/dylanmckay/
[AVRoxide]: https://avroxi.de/
[Patryk27]: https://github.com/Patryk27