#!/bin/sh

PLATFORMS=linux/amd64,linux/arm64

IMAGE=snowgoons/avr-rustbuilder
TAG=`date +nightly-%Y-%m-%d`

docker buildx rm avr-rustbuilder-builder || true
docker buildx create --name avr-rustbuilder-builder
docker buildx create --append --name avr-rustbuilder-builder --platform linux/amd64   hp-node0
docker buildx create --append --name avr-rustbuilder-builder --platform linux/arm/v7  pisicuta
docker buildx create --append --name avr-rustbuilder-builder --platform linux/arm64   arm-node0

docker buildx use avr-rustbuilder-builder
docker buildx build --platform ${PLATFORMS} -t ${IMAGE}:${TAG} --push .
