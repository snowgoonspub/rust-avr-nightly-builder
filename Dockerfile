FROM snowgoons/rustbuilder:rustc-1.68.2

RUN echo 'Acquire::http { Proxy "http://apt-cacher.svc.int.snowgoons.ro:3142"; };' > /etc/apt/apt.conf.d/01proxy
RUN echo 'Acquire::https::proxy "DIRECT";' >> /etc/apt/apt.conf.d/01proxy
RUN apt-get update

## REMOVED 2022-05-10
##
## We don't need this any more, because the regular `nightly` toolchain
## now works.
##
## Dependencies for building Rust
#RUN apt-get install -y gcc-avr binutils-avr avr-libc wget lsb-release software-properties-common
#RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - > /etc/apt/trusted.gpg.d/kitware.gpg
#RUN apt-add-repository "deb https://apt.kitware.com/ubuntu/ $(lsb_release -cs) main"
#RUN apt-get update && apt-get install -y cmake ninja-build

# This will build the custom 'snowgoons' toolchain
#WORKDIR /home/snowgoons
#ADD patches ./patches
#ADD build ./build
#ADD build.sh .
#RUN ./build.sh --cleanup

# Still good to have an image with a 'known good' version of nightly tho
RUN rustup install nightly # 2023-04-18
RUN rustup component add rust-src --toolchain `rustup show | grep nightly`
RUN rustup install nightly-2022-10-22 # last working AVR version
RUN rustup component add rust-src --toolchain `rustup show | grep nightly-2022-10-22`
