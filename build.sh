#!/bin/bash

RUSTSRC=https://github.com/rust-lang/rust

if [ "X${1}" = "X--noupdate" ]; then
  UPDATE=
  shift
else
  UPDATE=true
fi

if [ "X${1}" = "X--cleanup" ]; then
  CLEANUP=true
  shift
else
  CLEANUP=
fi

TOOLCHAIN=${1:-snowgoons}


# First download or update the latest Rust source
echo "Obtaining latest Rust source"
(
  cd build || exit

  if [ -d rust ]; then
    (
      cd rust || exit
      if [ ${UPDATE} ]; then
        echo "Pulling latest sources (use --noupdate to skip this)"
        git pull
      fi
    )
  else
    git clone ${RUSTSRC}
  fi
)

# Now update the submodules
echo "Updating submodule sources"
(
  cd build/rust || exit
  git submodule update --init --recursive
)

# Create a config.toml if one is not already there
if [ ! -f build/rust/config.toml ]; then
  echo "Creating config file"
  (
    cd build/rust || exit
    cp config.toml.example config.toml
    for PATCH in  ../../patches/Config*.patch ; do
      patch < ${PATCH}
    done
  )
fi

# Apply LLVM patches
echo "Patching LLVM"
(
  cd build/rust || exit
  (
    echo "Cleaning local changes"
    cd src/llvm-project
    git restore .
    git clean -f
  )
  for PATCH in  ../../patches/LLVM-*.patch ; do
    patch --forward -u -p0 -r -  < ${PATCH}
  done
)

# Build!
echo "Building Rust Toolchain"
(
  cd build/rust || exit
  ./x.py build --stage 2 library/core library/proc_macro
)

# And finally...  Link the toolchain in
echo "Linking toolchain as ${TOOLCHAIN}"
(
  # shellcheck disable=SC2006
  ARCH=$(rustc -vV | grep "^host:" | sed -e "s/host: //")
  cp -R "build/rust/build/${ARCH}/stage2" toolchain || exit
  rustup toolchain link "${TOOLCHAIN}" toolchain
)

# Cleanup anything that is not needed for a running toolchain.
# This is intended for when we are building a containerised toolchain.
if [ ${CLEANUP} ]; then
  rm -rf build/rust/build
  rm -rf build/rust/src/llvm-project
fi